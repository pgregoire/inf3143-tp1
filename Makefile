

USE=		sh ../../tools/use-4.2.0/bin/use
USEFLAGS=	-nogui -nr -qv

MODEL=	./../inf3143_171_tp1_public/model.use

RUN_TEST=\
	testdir() { \
		for test in "$${1}"/*.in; do \
			fn=$$(basename -- "$${test}"); \
			bn=$$(basename -- "$${test}" .in); \
			cp "$${test}" .; \
			${USE} ${USEFLAGS} use "$${fn}" >"$${bn}.res"; \
			res=$$(cat "$${1}/$${bn}.res"); \
			grep -- "$${res}" "$${bn}.res" >"$${bn}.diff"; \
			got=$$(cat "$${bn}.diff"); \
			printf '%s ' "$${bn}" >&2; \
			if [ X != X"$${got}" ]; then \
				printf ' OK\n' >&2; \
			else \
				printf 'FAIL\n' >&2; \
			fi; \
		done; \
	}

check: constraints.use
	[ ! -d ./tests_out ] || rm -rf ./tests_out
	mkdir ./tests_out
	(cd ./tests_out; \
	 cat ${MODEL} ../constraints.use >./use; \
	\
	 ${RUN_TEST}; \
	  testdir ../tests;)
#	  testdir ../inf3143_171_tp1_public/tests; \

use:
	[ -d tests_out ] || mkdir tests_out
	(cd tests_out; \
	 cat ${MODEL} ../constraints.use >./use; \
	 ${USE} ./use ${use};)

upload:
	rsync -a ../../inf3143 malt.labunix:
